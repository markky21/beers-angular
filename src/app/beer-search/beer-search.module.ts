import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeerSearchComponent } from './beer-search.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [BeerSearchComponent],
  exports: [
    BeerSearchComponent
  ]
})
export class BeerSearchModule { }
