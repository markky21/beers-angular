import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl } from '@angular/forms';
import {PunkService} from './../punk.service';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'app-beer-search',
  templateUrl: './beer-search.component.html',
  styleUrls: ['./beer-search.component.scss']
})
export class BeerSearchComponent implements OnInit {

  searchForm: FormGroup;

  constructor(private punkService: PunkService) { }

  ngOnInit() {
    this.searchForm = new FormGroup({
      'query': new FormControl(),
    });

    this.searchForm.get('query').valueChanges
      .filter(query => query.length >= 3)
      .distinctUntilChanged()
      .debounceTime(400)
      .subscribe(query => {
        if (query && query !== '') {
          this.punkService.searchBeers(query);
          console.log(query);
        }
      });
  }

}
