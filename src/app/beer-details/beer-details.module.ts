import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterModule} from '@angular/router';

import {BeerDetailsComponent} from './beer-details.component';
import {BeerRecommendedComponent} from './beer-recommended/beer-recommended.component';


@NgModule({
  imports: [
    CommonModule,
    LazyLoadImageModule,
    BrowserAnimationsModule,
    RouterModule,
  ],
  declarations: [BeerDetailsComponent, BeerRecommendedComponent],
  exports: [
    BeerDetailsComponent
  ]
})
export class BeerDetailsModule {
}
