import {Component, OnInit} from '@angular/core';
import {PunkService} from './../punk.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {fadeInTrigger} from './../_animations/fadeInAnimation';
import {fadeInOutTrigger} from './../_animations/fadeInOutAnimation';
import {itemStateTrigger} from './../_animations/listAnimation';

@Component({
  selector: 'app-beer-details',
  templateUrl: './beer-details.component.html',
  styleUrls: ['./beer-details.component.scss'],
  animations: [fadeInTrigger, itemStateTrigger, fadeInOutTrigger]
})
export class BeerDetailsComponent implements OnInit {

  beer;
  beerId: number;
  recommended: Array<any>;

  constructor(private punkService: PunkService, private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.beerId = this.activatedRoute.snapshot.params.id
    if(this.beerId){
      this.getBeer();
    }

    this.router.events.subscribe((rout) => {
      if (rout instanceof NavigationEnd) {
         const beerIdMatch = /\/beer-details\/(\d+)/.exec(rout.url);
        this.beerId = beerIdMatch ? parseInt(beerIdMatch[1]) : null;

        if(typeof this.beerId === 'number'){
          this.getBeer();
        }
      }
    });
  }

  getBeer () {
    this.punkService.getBeerByID(this.beerId).subscribe(beer => {
      this.beer = beer[0];
      this.punkService.searchByHops(this.getHops(this.beer)).subscribe(recommended => {
        this.recommended = recommended;
      });
    });
  }

  getHops(beer = this.beer) {
    const hops = beer.ingredients.hops;
    const hopsArr = [];

    for (let i = 0; i < hops.length; i++) {
      hopsArr.push(hops[i].name);
    }
    return hopsArr;
  }

  stopPropagation($event) {
    $event.stopPropagation();
  }
}
