import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-beer-recommended',
  templateUrl: './beer-recommended.component.html',
  styleUrls: ['./beer-recommended.component.scss']
})
export class BeerRecommendedComponent implements OnInit {
  @Input() beer;

  constructor() {
  }

  ngOnInit() {
  }

}
