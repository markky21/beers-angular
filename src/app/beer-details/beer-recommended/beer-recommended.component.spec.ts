import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerRecommendedComponent } from './beer-recommended.component';

describe('BeerRecommendedComponent', () => {
  let component: BeerRecommendedComponent;
  let fixture: ComponentFixture<BeerRecommendedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerRecommendedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerRecommendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
