import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {routerModule} from './app.routing';

import {BeerCardModule} from './beer-card/beer-card.module';
import {BeerDetailsModule} from './beer-details/beer-details.module';
import {BeerSearchModule} from './beer-search/beer-search.module';

import {PunkService} from './punk.service'

import {AppComponent} from './app.component';
import { BlankComponent } from './blank/blank.component';


@NgModule({
  declarations: [
    AppComponent,
    BlankComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BeerCardModule,
    BeerDetailsModule,
    BeerSearchModule,
    BrowserAnimationsModule,
    routerModule,
  ],
  providers: [
    PunkService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
