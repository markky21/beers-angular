import {Component, OnInit} from '@angular/core';
import {PunkService} from './punk.service';
import {Subscription} from 'rxjs/Subscription';

import {itemStateTrigger} from './_animations/listAnimation';
import {fadeInOutTrigger} from './_animations/fadeInOutAnimation';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [itemStateTrigger, fadeInOutTrigger],
})
export class AppComponent implements OnInit {

  beers: Array<any> = [];
  beersShowed: Array<any> = []
  beersSubscription: Subscription;
  allBeersShowed: boolean = false;
  showBeerLoader: boolean = true;
  noMoreToShow: boolean = false;

  constructor(private punkService: PunkService) {


    this.beersSubscription = this.punkService.getBeersObservable()
      .subscribe((beers) => {
        this.beers = beers;
        this.showBeerLoader = false;
        if (beers.length > 0) {
          this.showNewBeers();
          this.noMoreToShow = false;
        } else {
          this.noMoreToShow = true;
        }
      });

    this.beersSubscription = this.punkService.getNewQueryObservable()
      .subscribe((query) => {
        this.beersShowed = [];
      });

  }

  ngOnInit() {
    this.punkService.getBeers();

    this.infiniteScroll();
  }

  showNewBeers() {
    let i = 0;
    this.allBeersShowed = false;
    const pushNextBeer = () => {
      if (i < this.beers.length) {
        setTimeout(() => {
          this.beersShowed.push(this.beers[i]);
          i++;
          pushNextBeer();
        }, 100);
      } else {
        this.allBeersShowed = true;
      }
    }
    pushNextBeer();
  }

  showNextPage() {
    this.showBeerLoader = true;
    this.punkService.getNextPage();
  }

  infiniteScroll() {
    window.addEventListener('scroll', (e) => {
      if (this.allBeersShowed) {
        const bodyH = document.body.clientHeight;
        const windowH = window.innerHeight;
        const currentPos = document.documentElement.scrollTop;

        if (bodyH - (currentPos + windowH) < 50) {
          this.allBeersShowed = false;
          this.showNextPage();
        }
      }
    }, false);
  }

}
