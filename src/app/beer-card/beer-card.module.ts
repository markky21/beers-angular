import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { BeerCardComponent } from './beer-card.component';

@NgModule({
  imports: [
    CommonModule,
    LazyLoadImageModule,
  ],
  declarations: [
    BeerCardComponent],
  exports: [
    BeerCardComponent,
  ]
})
export class BeerCardModule { }
