import {Component, OnInit, Input} from '@angular/core';
import {PunkService} from './../punk.service';

@Component({
  selector: 'app-beer-card',
  templateUrl: './beer-card.component.html',
  styleUrls: ['./beer-card.component.scss'],

})
export class BeerCardComponent implements OnInit {

  @Input() beer;

  constructor(private punkService: PunkService) {
  }

  ngOnInit() {
  }

}
