import {RouterModule, Routes} from '@angular/router';
import {BlankComponent} from './blank/blank.component';
import {BeerDetailsComponent} from './beer-details/beer-details.component';

const routesConfig: Routes = [
  {path: '', component: BlankComponent},
  {path: 'beer-details/:id', component: BeerDetailsComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routerModule = RouterModule.forRoot(routesConfig);
