import {trigger, style, transition, animate} from '@angular/animations';

export const fadeInOutTrigger = trigger('fadeInOut', [

  transition(':enter', [

    style({
      opacity: 0,
    }),
    animate('300ms ease-out', style({
      opacity: 1,
    }))

  ]),

  transition(':leave', [

    style({
      opacity: 1,
    }),
    animate('230ms ease-out', style({
      opacity: 0,
    }))

  ])

]);
