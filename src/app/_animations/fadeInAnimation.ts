import {trigger, style, transition, animate} from '@angular/animations';

export const fadeInTrigger = trigger('fadeIn', [

  transition(':enter', [

    style({
      opacity: 0,
      transform: 'translateY(100px)'
    }),
    animate('500ms ease-out', style({
      opacity: 1,
      transform: 'translateY(0)'
    }))

  ]),

]);
