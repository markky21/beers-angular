import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';


@Injectable()
export class PunkService {

  private searchedBeers = new Subject();
  private newQuery = new Subject();

  page: number = 1;
  currentQuery: string = '';
  punkApiUrl = 'https://api.punkapi.com/v2/beers?';

  constructor(private http: Http) {
  }


  setPage(page = ++this.page) {
    this.page = page;
  }

  getUrl(query, page) {
    const queryUri = query ? `beer_name=${encodeURI(query.replace(' ', '_'))}&` : '';
    const pageUri = `page=${page}&`;
    return `${this.punkApiUrl + queryUri + pageUri}per_page=20`;
  }

  getNextPage() {
    this.setPage(++this.page);
    this.searchBeers(this.currentQuery, this.page)
  }

  getBeers() {
    this.searchBeers(null, this.page);
  }

  searchBeers(query, page = 1) {
    if (this.currentQuery !== query) {
      this.setNewQuery(query);
    }
    this.http.get(this.getUrl(query, page))
      .map((response) => {
        this.searchedBeers.next(response.json())
      }).subscribe();
  }

  searchByHops(hops) {
    const hop = hops[Math.floor(Math.random() * hops.length)];
    const url = `${this.punkApiUrl}hops=${encodeURI(hop.replace(' ', '_'))}&page=1&per_page=3`;
    return this.http.get(url)
      .map((response) => {
        return response = response.json();
      });
  }

  getBeerByID(beerId) {
    const url = `${this.punkApiUrl}ids=${beerId}`;
    return this.http.get(url)
      .map((response) => {
        return response = response.json();
      });
  }

  setNewQuery(query) {
    this.currentQuery = query;
    this.newQuery.next(query);
  }

  getNewQueryObservable(): Observable<any> {
    return this.newQuery.asObservable();
  }

  getBeersObservable(): Observable<any> {
    return this.searchedBeers.asObservable();
  }
}
